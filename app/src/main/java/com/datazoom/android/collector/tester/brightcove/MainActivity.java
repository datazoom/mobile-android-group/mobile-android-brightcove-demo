package com.datazoom.android.collector.tester.brightcove;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.brightcove.player.model.DeliveryType;
import com.brightcove.player.model.Video;
import com.brightcove.player.view.BrightcoveExoPlayerVideoView;
import com.brightcove.player.view.BrightcoveVideoView;
import com.datazoom.android.collector.basecollector.connection_manager.DZBeaconConnector;
import com.datazoom.android.collector.basecollector.event_collector.collector.DZEventCollector;
import com.datazoom.android.collector.basecollector.model.DatazoomConfig;
import com.datazoom.android.collector.basecollector.model.dz_events.Event;
import com.datazoom.android.collector.brightcove.BrightcovePlayerCollector;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getCanonicalName();

    @BindView(R.id.brightCodeVideoView)
    BrightcoveVideoView brightcoveVideoView;

    @BindView(R.id.txtUrl)
    EditText txtUrl;

    @BindView(R.id.txtConfiguration)
    EditText txtConfiguration;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @BindView(R.id.btnPush)
    Button btnPush;

    @BindView(R.id.txtVersion)
    TextView txtVersion;

    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        btnPush.setVisibility(View.GONE);
        txtVersion.setText("Demo - " + BuildConfig.VERSION_NAME + " | Library - " + com.datazoom.android.collector.brightcove.BuildConfig.VERSION_NAME + " | Framework - " + com.datazoom.android.collector.brightcove.BuildConfig.VERSION_NAME);


        btnSubmit.setOnClickListener(v -> {
            btnPush.setVisibility(View.VISIBLE);
            btnSubmit.setVisibility(View.GONE);
            btnSubmit.setEnabled(false);
            progressBar.setVisibility(View.VISIBLE);
            String configId = txtConfiguration.getText().toString();
            String configUrl = txtUrl.getText().toString();

            BrightcovePlayerCollector.create(brightcoveVideoView, this)
                    .setConfig(new DatazoomConfig(configId, configUrl))
                    .connect(new DZBeaconConnector.ConnectionListener() {
                        @Override
                        public void onSuccess(DZEventCollector dzEventCollector) {
                            try {
                                dzEventCollector.setDatazoomMetadata(new JSONArray( "[{\"customPlayerName\": \"Brightcove Player\"},{\"customDomain\": \"devplatform.io\"}]  "));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            Event event = new Event("SDKLoaded",new JSONArray());
                            dzEventCollector.addCustomEvent(event);

                            btnPush.setOnClickListener(v -> {

                                try {
                                    Event event1 = new Event("btnPush",new JSONArray( "[{\"customPlay\": \"true\"}]  ") );
                                    dzEventCollector.addCustomEvent(event1);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            });
                            progressBar.setVisibility(View.GONE);
                            brightcoveVideoView.setVisibility(View.VISIBLE);
                            final MediaController mediacontroller = new MediaController(MainActivity.this) {
                                @Override
                                public void hide() {
                                    this.show();
                                }
                            };
                            mediacontroller.setAnchorView(brightcoveVideoView);
                            brightcoveVideoView.setMediaController(mediacontroller);
//                            mediacontroller.show();

                            brightcoveVideoView.add(Video.createVideo("https://www.radiantmediaplayer.com/media/bbb-360p.mp4", DeliveryType.MP4));

                            brightcoveVideoView.start();
                        }

                        @Override
                        public void onError(Throwable t) {
                            progressBar.setVisibility(View.GONE);
                            btnSubmit.setEnabled(true);
                        }
                    });

        });

        try {
            LogMonitor.startAppendingLogsTo(new File(getFilesDir(), "datazoom-log.log"));
        } catch (IOException e) {
            Log.e(TAG, "Cannot create file:datazoom-log.log");
        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogMonitor.stopMonitoring();
    }
}
